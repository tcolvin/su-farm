from libcpp.string cimport string
from libcpp cimport bool
import numpy as np
cimport numpy as np
#from ctypes import *
import ctypes
from libcpp.vector cimport vector
from libcpp.map cimport map
from libcpp.utility cimport pair

from cython.operator cimport dereference as deref
from cython.operator cimport preincrement as inc

#  http://stackoverflow.com/questions/19057439/missing-numpy-arrayobject-h-while-compiling-pyx-file
# https://github.com/cython/cython/wiki/tutorials-NumpyPointerToC

from cpython.buffer cimport Py_buffer
from libc.string cimport memcpy

import datetime as dt
import sys


#cdef extern from "classes.h":
#    cdef cppclass BaseClass:
#        BaseClass(BaseClass *parent)
#    
#    cdef cppclass MainClass:
#        MainClass(BaseClass *parent)
#        void accumulate(int new_number)
#        int get_total()
#
#
#
### ~~~~~~~~~~~~~~~~~~~~~ Grid3D CLASS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    
cdef extern from "Grid3D.h":
    cdef cppclass Grid3D:
        Grid3D()
        Grid3D(const Grid3D &obj) # Copy constructor
        Grid3D(map[int, map[int, map[int,double]]] SpatialProbabilty_in)
        map[int, map[int, map[int,double]]] getGrid() const
        Grid3D removeNoDanger(const Grid3D &obj)

        # Grid3D* operator+(Grid3D*)
        Grid3D operator+(const Grid3D &obj)
        Grid3D operator*(double k)
        bool operator<=(const Grid3D &obj)


cdef class PyGrid3D:
    cdef Grid3D *thisptr                    # hold a C++ instance which we're wrapping
    
    # def __cinit__(self, dict SpatialProbabilty_in):
    #     self.thisptr = new Grid3D(SpatialProbabilty_in)
    
    # def __cinit__(self, PyGrid3D obj=None):
    #     if obj:
    #         # Call the copy constructor.  The 'new' is very important; without it you will get errors when deallocating:
    #         #  malloc: *** error for object 0x104b0c438: pointer being freed was not allocated
    #         self.thisptr = new Grid3D(obj.thisptr[0])

    #     else:
    #         self.thisptr = new Grid3D()

    def __cinit__(self, **kwargs):
        if 'grid' in kwargs:
            obj1 = <PyGrid3D> kwargs['grid']
            # Call the copy constructor.  The 'new' is very important; without it you will get errors when deallocating:
            #  malloc: *** error for object 0x104b0c438: pointer being freed was not allocated
            self.thisptr = new Grid3D(obj1.thisptr[0])
        elif 'dict' in kwargs:
            obj = <map[int, map[int, map[int,double]]]> kwargs['dict']
            self.thisptr = new Grid3D(obj)
        else:
            self.thisptr = new Grid3D()

    def __dealloc__(self):
        del self.thisptr

    # cdef Grid3D add(self, Grid3D one, Grid3D two):
    #     return (one + two)

    # THE CRUX!!! If you want this to work, you MUST declare the type of self as PyGrid3D.
    #   Otherwise, (self.thisptr[0] + obj.thisptr[0]) will try to convert the Grid3D objects into PyObjects
    def __add__(PyGrid3D self, PyGrid3D obj):
        ans = PyGrid3D()
        ans.thisptr[0] = (self.thisptr[0] + obj.thisptr[0])
        return ans

    def __mul__(PyGrid3D self, double k):
        ans = PyGrid3D()
        ans.thisptr[0] = (self.thisptr[0] * k)
        return ans    

    # def __le__(PyGrid3D self, PyGrid3D obj):
    #     return self.thisptr[0] <= obj.thisptr[0]

    def __richcmp__(PyGrid3D self, PyGrid3D obj, int op):
        # This is a cython-specific method that must be used for the "rich" comparisons
        # Takes an integer code (op) to specify which rich comparison is desired
        # <   0, # ==  2, # >   4, # <=  1, # !=  3, # >=  5
        if op == 1:
            return self.thisptr[0] <= obj.thisptr[0]
        else:
            raise NotImplementedError

    def getGrid(self):
        return self.thisptr.getGrid()

    def removeNoDanger(PyGrid3D self, PyGrid3D obj):
        ans = PyGrid3D()
        ans.thisptr[0] = self.thisptr.removeNoDanger(obj.thisptr[0])
        return ans
        
    # Grid3D removeNoDanger(const Grid3D &obj);


## ~~~~~~~~~~~~~~~~~~~~~ SKYGRID CLASS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# cdef extern from "SkyGrid.h" namespace "SkyGrid":
cdef extern from "SkyGrid.h":
    cdef cppclass SkyGrid:
#        SkyGrid(char *CapeLrhcFile, double xBinLength_in, double yBinLength_in, double zBinHeight_in)
        SkyGrid(PointCloud *newCloud, double xBinLength_in, double yBinLength_in, double zBinHeight_in)
        SkyGrid(double xBinLength_in, double yBinLength_in, double zBinHeight_in,
                 double all_points_UTC_in, double all_points_delta_t_in,
                 double all_points_launchLat_in, double all_points_launchLon_in, double all_points_launchAzimuth_in)

        double getLaunchLat()
        double getLaunchLon()
        double getLaunchAzimuth()
        double getInitialUTC()

        int getProbImpactCode()
        int getProbCasualtyCode()
        int getProbCatastropheCode()

#        void PythonDebrisIntoGrid(void *flatPointArray, int numPieces, void *numTimeSteps, int maxTime, double deltaT,  # arguments for assembling the points
#                                  double NewInitialUTC, double timeOffsetSec, double launchLat, double launchLon, double launchAzimuth)  #encorporation
        
        void PythonDebrisIntoGrid(PointCloud *incomingCloud)


#        void ExportBinnedDebrisGoogleEarth(char *googleEarthFile, int yyyy, int mm, int dd, int hour, int min)

#        void generateAllPointsFromKDE(double deltaXY)
        
        void generateAllPointsFromGrid()
        
        int identifyYourself()
        int getNumRange()

        
        # Probability stuff
#        void ConvertToEmptyProbability()
        void ConvertToProbability(double weight)
#        void weightedCombine(SkyGrid *newSkyGrid, double weight)
#        bool isTotalProbabilityGood()

#        double generateAllPointsFromSimpleHistogram(double thresh, int Ntotal, int numEventsSimulated,  double pFail)

#        double generateAllPointsFromASH(vector[int] numberOfPiecesMeanArray_in, vector[double] arefMeanList_in, int numDebIX, double thresh, double pFail)
        
        
        void ASH2(double h1, double h2)
        double WTF()

        vector[double] createEmptyAircraftDensityMap()
        void populateAircraftDensityMap(void *densityMapArray, int numElements)

        void UploadAircraftTrackMap(map[int,pair[vector[vector[double]], string]] AircraftTrackMap, int aircraftTrackDeltaTSec)
        void UploadAircraftPropertiesMap(map[string,map[string,double]] AircraftPropertiesMap)

        map[int, double] CalculateRiskToIndividualAircraft_OnTheFly(vector[int] numberOfPiecesMean, vector[double] arefMean, int secondsFromMidnightUTC,
                                                                     double h1_in, double h2_in)
        
#        vector[vector[double]] SendGridToPython(int tx_desired)
        map[double, map[double, map[double,double]]] SendGridToPython(int tx_desired)
        map[int, map[int, map[int,int] ] ] SendHistogramToPython(int betaID, int tx_desired)
        map[int, map[int, map[int,double]]] SendProbabilitiesToPython(int betaID, int tx_desired, int probDesired)
        map[int, map[int, map[int,double]]] SendHazardPointsToPython()


        # For debugging
        map[int, map[int, map[int,double]]] getSpatialProbabilty()
        Grid3D generateSpatialProbability(int whichProb, int J_maxTimeStep, int f_startTimeStep)
        # PyGrid3D generateSpatialProbability(int whichProb, int J_maxTimeStep, int f_startTimeStep)
 
        void generateHazardProbabilities(vector[int] numberOfPiecesMean)
        # double generateAllPoints_CumulativeTJC(double thresh, int whichProb)
        double generateAllPoints_CumulativeFAA(double thresh, int whichProb, double pFail)

        # double applyCumulativeThreshold(Grid3D grid, double thresh, vector[int] txVec)
        double applyCumulativeThreshold(const Grid3D &grid, double thresh, vector[int] txVec)


        ### ==== Graveyard
        # void DumpGridToMatlab(char *fileName)
        # map[int, map[int, map[int,double]]] projectSpatialProbabilityFAA(double newDeltaXY, double newDeltaZ)




#        np.ndarray[np.double_t, ndim=2] createEmptyAircraftDensityMap()





cdef class PySkyGrid:
    cdef SkyGrid *thisptr                    # hold a C++ instance which we're wrapping
    
    # def __cinit__(self, dict curMission, PyPointCloud incoming):
    #     xBinLength_in = curMission['deltaXY']
    #     yBinLength_in = curMission['deltaXY']
    #     zBinHeight_in = curMission['deltaZ']
    #     self.thisptr = new SkyGrid( incoming.thisptr,  xBinLength_in,  yBinLength_in,  zBinHeight_in)
    

    def __cinit__(self, **kwargs):
        curMission = kwargs['curMission']
        xBinLength_in = curMission['deltaXY']
        yBinLength_in = curMission['deltaXY']
        zBinHeight_in = curMission['deltaZ']

        if ('pointCloud' in kwargs):
            incoming = <PyPointCloud> kwargs['pointCloud']
            self.thisptr = new SkyGrid( incoming.thisptr,  xBinLength_in,  yBinLength_in,  zBinHeight_in)
        else:
            # Make an empty one
            all_points_UTC_in = curMission['initialUTC']
            all_points_delta_t_in = curMission['deltaTFail']  # footprint will have same time steps as simulated explosions, 
                                                              #  otherwise there would be empty timesteps 
            all_points_launchLat_in = curMission['launchLat']
            all_points_launchLon_in = curMission['launchLon']
            all_points_launchAzimuth_in = curMission['launchAzimuth']

            self.thisptr = new SkyGrid(xBinLength_in, yBinLength_in, zBinHeight_in,
                 all_points_UTC_in, all_points_delta_t_in,
                 all_points_launchLat_in, all_points_launchLon_in, all_points_launchAzimuth_in)

    def __dealloc__(self):
        del self.thisptr

    def getLaunchLat(self):
        return self.thisptr.getLaunchLat()

    def getLaunchLon(self):
        return self.thisptr.getLaunchLon()
    
    def getLaunchAzimuth(self):
        return self.thisptr.getLaunchAzimuth()

    def getInitialUTC(self):
        return self.thisptr.getInitialUTC()
    
    def getNumRange(self):
        return self.thisptr.getNumRange()

    def getProbImpactCode(self):
        return self.thisptr.getProbImpactCode()

    def getProbCasualtyCode(self):
        return self.thisptr.getProbCasualtyCode()

    def getProbCatastropheCode(self):
        return self.thisptr.getProbCatastropheCode()
    
    def IncorporatePointCloudIntoGrid(self, PyPointCloud incomingCloud):
        self.thisptr.PythonDebrisIntoGrid(incomingCloud.thisptr)

#    def ExportToGoogleEarth(self, char *googleEarthFile, int yyyy, int mm, int dd, int hour, int min):
#        self.thisptr.ExportBinnedDebrisGoogleEarth(googleEarthFile, yyyy, mm, dd, hour, min)

#    def generateAllPointsFromKDE(self, double deltaXY):
#        self.thisptr.generateAllPointsFromKDE(deltaXY)

#    def generateAllPointsFromGrid(self):
#        self.thisptr.generateAllPointsFromGrid()
    
    def IdentifyYourself(self):
        self.thisptr.identifyYourself()

#    def ConvertToEmptyProbability(self):
#        self.thisptr.ConvertToEmptyProbability()

#    def ConvertToProbability(self,  double weight):
#        self.thisptr.ConvertToProbability(weight)

#    def weightedCombine(self, PySkyGrid newSkyGrid, double weight):
#        self.thisptr.weightedCombine(newSkyGrid.thisptr, weight)

#    def isTotalProbabilityGood(self):
#        return self.thisptr.isTotalProbabilityGood()

#    def generateAllPointsFromSimpleHistogram(self, double thresh, int Ntotal, int numEventsSimulated,  double pFail):
#        return self.thisptr.generateAllPointsFromSimpleHistogram(thresh, Ntotal, numEventsSimulated, pFail)
    
    def generateASH(self, h1, h2):
        self.thisptr.ASH2(h1, h2)

    
#    def generateAllPoints(self, numberOfPiecesMeanList, arefMeanList, double thresh, double pFail):
#        return self.thisptr.generateAllPointsFromASH(numberOfPiecesMeanList, arefMeanList, len(numberOfPiecesMeanList), thresh, pFail)
    
#    # Doesn't look like this gets used anymore (we call ASH separately at the moment.  Comment out.
#    def generateAllPointsFromASH(self, numberOfPiecesMeanList, arefMeanList, double thresh, double pFail,
#                                 double h1, double h2, int numDebrisPerIXSimulated):
#
#        self.thisptr.ASH2(h1, h2, numDebrisPerIXSimulated)
#        return self.thisptr.generateAllPointsFromASH(numberOfPiecesMeanList, arefMeanList, len(numberOfPiecesMeanList), thresh, pFail)

    # TODO: If the code is working without this stuff, then send it to a graveyard.


    def SendGridToPython(self, int tx_desired):
        # Get the grid
        # Explicitly let cython know that the outer layer is going to be a dict/map and not a pair
        probGrid = <dict> self.thisptr.SendGridToPython(tx_desired)
                
        tempGrid = []
        for zval in probGrid:
            for lonVal in probGrid[zval]:
                for latVal in probGrid[zval][lonVal]:
                    probVal = probGrid[zval][lonVal][latVal]
                    tempGrid.append([zval, lonVal, latVal, probVal])
        probGrid = np.array(tempGrid)
        return probGrid

    def SendHistogramToPython(self, int betaID, int tx_desired):
        # map[double, map[double, map[double,int] ] ] 
        histogram = <dict> self.thisptr.SendHistogramToPython(betaID, tx_desired)
        return histogram

    def SendProbabilitiesToPython(self, int betaID, int tx_desired, int probDesired):
        # map[double, map[double, map[double,int] ] ] 
        ASH = <dict> self.thisptr.SendProbabilitiesToPython(betaID, tx_desired, probDesired)
        return ASH

    def SendHazardPointsToPython(self):
        # map[double, map[double, map[double,int] ] ] 
        pts = <dict> self.thisptr.SendHazardPointsToPython()
        return pts

        
#        # You have to tell Cython what to expect for the sub-iterators (so also defining curz for consistency)
#        cdef map[double, map[double, map[double,double]]].iterator curz
#        cdef map[double, map[double,double]].iterator curx
#        cdef map[double,double].iterator cury
#
#        probGrid = []                                           # This is the storage list which we will return
#        
#        curz = curGrid.begin()                                  # iterator over z-values
#        while curz != curGrid.end():                            # loop through iterators until you hit the end
#             
#            z_alt = float(deref(curz).first)                    # first is the altitude in km
#            curx = (deref(curz).second).begin()                 # iterator over x-values
#            while curx != (deref(curz).second).end():
#                
#                longitude = float(deref(curx).first)            # first is longitude in degrees
#                cury = deref(curx).second.begin()               # iterator over y-values
#                while cury != deref(curx).second.end():
#                    
#                    latitude = float(deref(cury).first)         # first is latitude in degrees
#                    probHere = float(deref(cury).second)        # second is probability of strike
#                    
#                    probGrid.append([z_alt, longitude, latitude, probHere])
#                
#                    inc(cury)                                   # all of these increments are PRE increments (see imports at top)
#                inc(curx)
#            inc(curz)
#        return probGrid

    def UploadAircraft(self, ACdict, aircraftTrackDeltaTSec):
#        # Just take one aircraft for now
#        keys = ACdict.keys()
#        singleAC = ACdict[keys[0]]
#        
#        testList = range(9)
#        cdef vector[double] ACvec = testList
#
#        cdef map[int,vector[vector[double]]] testMap = ACdict
#        self.thisptr.UploadSingleAircraft(testMap)
        self.thisptr.UploadAircraftTrackMap(ACdict, aircraftTrackDeltaTSec)

    def UploadAircraftPropertiesMap(self, incomingMap):
        self.thisptr.UploadAircraftPropertiesMap(incomingMap)
        
        
    def CalculateRiskToIndividualAircraft_OnTheFly(self, numberOfPiecesMeanList, arefMeanList, secondsFromMidnightUTC,
                                                   h1_in, h2_in):
        return self.thisptr.CalculateRiskToIndividualAircraft_OnTheFly(numberOfPiecesMeanList, arefMeanList, secondsFromMidnightUTC,
                                                                       h1_in, h2_in)

    # def GenerateSpatialProbability(self, whichProb, J_maxTimeStep, f_startTimeStep):
    #     #define PROB_IMPACT      1001
    #     #define PROB_CASUALTY    1002
    #     #define PROB_CATASTROPHE 1003
    #     return PyGrid3D(self.thisptr.generateSpatialProbability(whichProb, J_maxTimeStep, f_startTimeStep)[0])

    # First attempt that got it to compile and work
    def GenerateSpatialProbability(self, whichProb, J_maxTimeStep, f_startTimeStep):
        #define PROB_IMPACT      1001
        #define PROB_CASUALTY    1002
        #define PROB_CATASTROPHE 1003
        obj = PyGrid3D()
        obj.thisptr[0] = self.thisptr.generateSpatialProbability(whichProb, J_maxTimeStep, f_startTimeStep)
        return obj

    # What it used to be before i started returning a Grid3D object
    # cdef Grid3D GenerateSpatialProbability(self, whichProb, J_maxTimeStep, f_startTimeStep):
    #     #define PROB_IMPACT      1001
    #     #define PROB_CASUALTY    1002
    #     #define PROB_CATASTROPHE 1003
    #     return self.thisptr.generateSpatialProbability(whichProb, J_maxTimeStep, f_startTimeStep)
            
    def GetSpatialProbabilty(self):
        return self.thisptr.getSpatialProbabilty()

    def generateHazardProbabilities(self, numberOfPiecesMean):
        self.thisptr.generateHazardProbabilities(numberOfPiecesMean)
            
    def generateAllPoints_CumulativeFAA(self, double thresh, int whichProb, double pFail):
        return self.thisptr.generateAllPoints_CumulativeFAA(thresh, whichProb, pFail)

    def applyCumulativeThreshold(self, PyGrid3D curGrid, double thresh, vector[int] txVec):
        return self.thisptr.applyCumulativeThreshold(curGrid.thisptr[0], thresh, txVec)
            # double applyCumulativeThreshold(const Grid3D &grid, double thresh, vector[int] txVec)

    ### ==== Graveyard ====
    # def DumpGridToMatlab(self, char *filename):
    #     self.thisptr.DumpGridToMatlab(filename)
    # def GetSpatialProbabilty_Coarse(self, newDeltaXY, newDeltaZ):
    #     return self.thisptr.projectSpatialProbabilityFAA(newDeltaXY, newDeltaZ)
    # def generateAllPoints_CumulativeTJC(self, double thresh, int whichProb):
    #     return self.thisptr.generateAllPoints_CumulativeTJC(thresh, whichProb)
    # def createEmptyAircraftDensityMap(self):
    #     # cdef vector<double> tempAns = self.thisptr.createEmptyAircraftDensityMap()
    #     # i cannot figure out how to do this nicely, so it's going to become a slow-ass hack
    #     tempAns = self.thisptr.createEmptyAircraftDensityMap()
    #     numEntries = tempAns.size()
    #     # Allocate the memory
    #     latlonArray = np.zeros( (numEntries,))
    #     for ix in range(numEntries):
    #         latlonArray[ix] = tempAns[ix]
    #     latlonArray = latlonArray.reshape(numEntries/4, 4)
    #     return latlonArray
    # def populateAircraftDensityMap(self, np.ndarray[double, ndim = 1, mode="c"] densityMapArray, int numElements):
    #     if (numElements > 0) or (numElements == -1):
    #         self.thisptr.populateAircraftDensityMap(&densityMapArray[0], numElements)

# This is here IN THE HOPES that it will allow me to pass PointCloud / SkyGrid objects into the footprint class
# http://docs.cython.org/src/userguide/external_C_code.html

cdef extern from "PointCloud.h":
# cdef extern from "PointCloud.h" namespace "PointCloud":
    cdef cppclass PointCloud:
        PointCloud()
        PointCloud(vector[double] flatPointArray_in, vector[int] pointIdArray, int numPieces, vector[int] numTimeSteps_in, int maxTime, double deltaTsec,
                   double all_points_UTC_in, double all_points_delta_t_in, double timeOffsetSec,
                   double all_points_launchLat_in, double all_points_launchLon_in, double all_points_launchAzimuth_in,
                   vector[double] massArray, vector[double] areaArray, double reactionTimeSeconds, double NASkm)
    
    
        void ExportPointsToMatlab(char *fileName, double deltaZkm)
        int ChopAfterSeconds(int numSeconds)


cdef class PyPointCloud:
    cdef PointCloud *thisptr                    # hold a C++ instance which we're wrapping
    
    def __cinit__(self):
        self.thisptr = new PointCloud()
    
    def __cinit__(self, dict pcd, double secondsFromLaunch, dict curMission):
        
        # Will load up all the points starting from the timestep that corresponds to secondsFromLaunch (ie tfail)
        #   and will create points for them in the cloud all the out to secondsFromLaunch + debrisMaxTime.
        #   Once a point lands, all of its subsequent timesteps are counted as landed.
        debrisMaxTime           = curMission['debrisTimeLimitSec']  # used to be pcd['maxTime'], but want this time consistent across all clouds

        # Unless there is a reaction time + monitoring latency, then those will take the place of debrisMaxTime.
        reactionTimeSeconds     = curMission['reactionTimeSeconds']
        healthMonitoringLatency = curMission['healthMonitoringLatency']
        if reactionTimeSeconds <= 0:
            healthMonitoringLatency = 0.

        if (reactionTimeSeconds+healthMonitoringLatency) > debrisMaxTime:
            print "ERROR: debrisMaxTime is less than your reaction + latency time"
            sys.exit()

        debrisDeltaT            = curMission['deltaT']  # Should be the deltaT for the debris propagations, not the envelopes!
        NASkm                   = curMission['NASkm']
        
        self.thisptr = new PointCloud(pcd['flatPointArray'],    pcd['debrisID'],        pcd['numPieces'],       pcd['numTimeSteps'],
                                      debrisMaxTime,            pcd['deltaTsec'],
                                      pcd['UTC'],               debrisDeltaT,     secondsFromLaunch,
                                      pcd['launchLat'],         pcd['launchLon'],       pcd['launchAzimuth'],
                                      pcd['debrisMass'],        pcd['debrisArea'],      reactionTimeSeconds+healthMonitoringLatency,    NASkm)
    
    def __dealloc__(self):
        del self.thisptr

    def ExportPointsToMatlab(self, char *fileName, double deltaZkm):
        self.thisptr.ExportPointsToMatlab(fileName, deltaZkm)

    def ChopAfterSeconds(self, int numSeconds):
        return self.thisptr.ChopAfterSeconds(numSeconds)



## ~~~~~~~~~~~~~~~~~~~~~ FOOTPRINT CLASS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cdef extern from "Footprint3D.h":
    cdef cppclass Footprint3D:
        #        Footprint3D()
        Footprint3D(char *FootprintVectorFile)
        #Footprint3D(char *pointsFile, double bin_size_in)
        #void generate_footprint_at_timesteps()
#        Footprint3D(PointCloud &incoming)
        Footprint3D(PointCloud *incoming, double bin_size_in, double arm_length_in)  
        Footprint3D(PointCloud *incoming, double bin_size_in) # arm_length_in has default value, but need to let cython know like this.
        
        void store_footprint_as_vector(char *footprintFileName)
        int load_footprint_as_vector(char *footprintFileName)
        
        void setAzimuthDeg(double azimuth_in)
        void setLaunchLatDeg(double lat)
        void setLaunchLonDeg(double lon)
        
        double getLaunchLat()
        double getLaunchLon()
        double getLaunchAzimuth()
        
        double getNumRange()
        double getDeltaT()
        
        void ChangeAzimuthToDeg(double newAzimuth)
        void ChangeLaunchSiteToDeg(double gdlatIN, double lonIN)
        #void ShiftFootprintByMinutes(int HowManyMinutes)  # Don't use this. SlideFootprintBySeconds instead?

        void exportGoogleEarth(char *googleEarthFile, int yyyy, int mm, int dd, int hour, int min)
        void make_facet_files(char *folderName, int startTimeMinutes, int offsetTimeMinutes, int tstepMinutes)

        void MergeFootprintVectors(Footprint3D *incomingFP)
        void SmoothedOut(double newDeltaT, double arm_length_in)
        void SmoothedOut(double newDeltaT)
        void SmoothedOut()
        int ProjectAllPointsDown(double arm_length_in)
        int ProjectAllPointsDown()
        double ChopTimeAt(double seconds)

        void SlideFootprintBySeconds(int howManySeconds)


#        void exportGoogleEarth(char *googleEarthFile)




cdef class PyFootprint:
    cdef Footprint3D *thisptr                    # hold a C++ instance which we're wrapping

    # Typeless overloaded constructor
    def __cinit__(self, **kwargs):

        # Can either pass in a PySkyGrid object
        if ('skygrid' in kwargs):
            incoming = kwargs['skygrid']    # TODO: type check
            bin_size_in = -5    # This is for a pure PointCloud, which is kind of no longer allowed.  Must be SkyGrid.
            # Must first cast the incoming python object to be a known python object PySkyGrid, then can cast its thisptr to PointCloud

            # If you don't pass in an armLength, then the envelopes will be convex by default.  If you want a shorter arm though, go for it.
            if 'armLength' in kwargs:
                armLength = kwargs['armLength']     # [km]   TODO: type check
                self.thisptr = new Footprint3D(<PointCloud*> (<PySkyGrid>incoming).thisptr, bin_size_in, armLength)
            else:
                self.thisptr = new Footprint3D(<PointCloud*> (<PySkyGrid>incoming).thisptr, bin_size_in)

        # Or can pass in a filename for a previously generated footprint
        elif 'footprintFileName' in kwargs:
            footprintFileName = kwargs['footprintFileName']         # TODO: type check
            self.thisptr = new Footprint3D(footprintFileName)       # footprintFileName is a string here

        # Or you can be an idiot
        else:
            print "No valid PyFootprint constructor for kwargs: {0}".format(kwargs)
        
    def __dealloc__(self):
        del self.thisptr
    
    def StoreFootprintAsVector(self, char *footprintFileName):
        self.thisptr.store_footprint_as_vector(footprintFileName)
        
    def LoadFootprintAsVector(self, char *footprintFileName):
        return self.thisptr.load_footprint_as_vector(footprintFileName)
    
    def ExportGoogleEarth(self, char *googleEarthFile, int yyyy, int mm, int dd, int hour, int min):
        self.thisptr.exportGoogleEarth(googleEarthFile, yyyy, mm, dd, hour, min)

    def ChangeAzimuthToDeg(self, double newAzimuth):
        self.thisptr.ChangeAzimuthToDeg(newAzimuth)

    def ChangeLaunchSiteToDeg(self, double newLat, double newLon):
        self.thisptr.ChangeLaunchSiteToDeg(newLat,newLon)
    
    def SetAzimuthDeg(self, double newAzimuth):
        self.thisptr.setAzimuthDeg(newAzimuth)
    
    def SetLaunchLatDeg(self, double newLat):
        self.thisptr.setLaunchLatDeg(newLat)
    
    def SetLaunchLonDeg(self, double newLon):
        self.thisptr.setLaunchLonDeg(newLon)
                
    def GetAzimuthDeg(self):
        return self.thisptr.getLaunchAzimuth()
    
    def GetLaunchLatDeg(self):
        return self.thisptr.getLaunchLat()
    
    def GetLaunchLonDeg(self):
        return self.thisptr.getLaunchLon()
    
    def getNumRange(self):
        return self.thisptr.getNumRange()
    
    def getDeltaT(self):
        return self.thisptr.getDeltaT()

    # def ShiftFootprintByMinutes(self, int HowManyMinutes):
    #     self.thisptr.ShiftFootprintByMinutes(HowManyMinutes)
    
    def MergeFootprintVectors(self, PyFootprint incomingFP):
        self.thisptr.MergeFootprintVectors(incomingFP.thisptr)
    
    # def SmoothedOut(self, newDeltaT = -1.):
    #     self.thisptr.SmoothedOut(newDeltaT)

    def SmoothedOut(self, **kwargs):
        if len(kwargs) == 0:
            # Smooth things but keep the same delta_t as before.
            self.thisptr.SmoothedOut()
        else:
            if 'newDeltaT' in kwargs:
                newDeltaT = kwargs['newDeltaT']   # You passed in this newDeltaT
            else:
                newDeltaT = -1  # This means you didn't pass in deltaT but you DID request a new arm length
                                # Setting newDeltaT to an "invalid" value will leave the deltaT of the footprint untouched.

            if 'armLength' in kwargs:
                armLength = kwargs['armLength']
                self.thisptr.SmoothedOut(newDeltaT, armLength)
            else:
                self.thisptr.SmoothedOut(newDeltaT)



    
    def ProjectAllPointsDown(self, **kwargs):
        if 'armLength' in kwargs:
            armLength = kwargs['armLength']
            return self.thisptr.ProjectAllPointsDown(armLength)
        else:
            return self.thisptr.ProjectAllPointsDown()
    
    def ChopTimeAt(self, double seconds):
        return self.thisptr.ChopTimeAt(seconds)
        
    def MakeFacetFiles(self, folderName, int startTimeSeconds, int offsetTimeSeconds, int obsolete):
        print folderName
        self.thisptr.make_facet_files(folderName, startTimeSeconds, offsetTimeSeconds, obsolete)

    def SlideFootprintBySeconds(self, howManySeconds):
        self.thisptr.SlideFootprintBySeconds(howManySeconds)




## ~~~~~~~~~~~~~~~~~~~~~ TRAJECTORY CLASS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# This class exists here solely to turn a time/lon/lat/alt file into a google earth animation
#
# This class used to get used for all of the trajectory propagations, but now I just use
#   Francisco's fortran file and do all of the controlling from python/cython.
#
#
cdef extern from "Trajectory.h":
    cdef cppclass Trajectory:
        Trajectory()
        
        # These files read in the trajectories to plot
        void loadPrecomputedFile(string inFileName, bool isDegrees, bool isAltMeters)
        void loadDebris(vector[double] flatPointArray, vector[int] pointIdArray, vector[int] numTimeSteps, int maxTime, double deltaTsec, double timeOffsetSec, double reactionTimeSeconds)

        void setLaunchTime(int launch_year_in, int launch_month_in, int launch_day_in, int launch_hours_in, int launch_minutes_in, int launch_seconds_in)
        int write_to_google_earth_native(string basename, int printThisMany)



cdef class PyTrajectory:
    cdef Trajectory *thisptr                    # hold a C++ instance which we're wrapping

    # This takes a file of the format time/lon/lat/alt
    def __cinit__(self):
        self.thisptr = new Trajectory()
    
    def loadPrecomputedTrajectory(self, string inFileName, bool isDegrees, bool isAltMeters):
        self.thisptr.loadPrecomputedFile(inFileName, isDegrees, isAltMeters)
    
    def loadDebrisTrajectory(self, dict pcd, double secondsFromLaunch, dict curMission):
        reactionTimeSeconds     = curMission['reactionTimeSeconds']
        self.thisptr.loadDebris(pcd['flatPointArray'],    pcd['debrisID'],      pcd['numTimeSteps'],
                                pcd['maxTime'],           pcd['deltaTsec'],
                                secondsFromLaunch,        reactionTimeSeconds)
    

    def ExportGoogleEarth(self, outFileName, curDateTime):
        if curDateTime == []:
            # Use some default values that won't be confused for real dates
            yyyy = 1984
            mon  = 5
            day  = 27
            hr   = 12
            min  = 30
            sec  = 00
        else:
            yyyy = curDateTime.year
            mon  = curDateTime.month
            day  = curDateTime.day
            hr   = curDateTime.hour
            min  = curDateTime.minute
            sec  = curDateTime.second

        self.thisptr.setLaunchTime(yyyy, mon, day, hr, min, sec)
        self.thisptr.write_to_google_earth_native(outFileName, 1)






## ~~~~~~~~~~~~~~~~~~~~~ POINT CLASS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# This class exists solely for debugging purposes.
# You should never actually need to use this directly from Python.
#
#
cdef extern from "Point.h":
    cdef cppclass Point:
        Point(double gdLatIN, double lonIN, double z_in, double R_local_in)    #(rad, rad, km, km)

cdef class PyPoint:
    cdef Point *thisptr                    # hold a C++ instance which we're wrapping
    
    # Constructor.  R_local is no longer used; use anything.
    def __cinit__(self, gdLatIN, lonIN, z_in):
        R_local = -1
        self.thisptr = new Point(gdLatIN, lonIN, z_in, R_local)

















    